import React, { Component } from "react";
import bg_game from "../assets/bgGame.png";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export default class Ex_XucXac extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          width: "100vw",
          height: "100vh",
        }}
      >
        <h1 className="pt-5 text-monospace text-black"> GAME TÀI XỈU</h1>
        <XucXac />
        <KetQua />
      </div>
    );
  }
}
