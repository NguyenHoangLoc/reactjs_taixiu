import { PLAY_GAME, TAI, XIU, CHON } from "../Constants/xucXacConstant";
let initialState = {
  arrXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  tongSoLanChoi: 0,
  tongSoLanThang: 0,
  ketQua: null,
};
export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let newArrXucXac = state.arrXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${random}.png`,
          giaTri: random,
        };
      });
      let newTongSoLanChoi = state.tongSoLanChoi;
      newTongSoLanChoi++;

      let tongDiem = 0;
      let newTongSoLanThang = state.tongSoLanThang;
      state.arrXucXac.forEach((item) => {
        tongDiem += item.giaTri;
      });
      if (tongDiem >= 11) {
        if (state.luaChon === TAI) {
          newTongSoLanThang++;
          state.ketQua = "YOU WIN";
        } else {
          state.ketQua = "YOU LOSE";
        }
      } else {
        if (state.luaChon === XIU) {
          newTongSoLanThang++;
          state.ketQua = "YOU WIN";
        } else {
          state.ketQua = "YOU LOSE";
        }
      }

      return {
        ...state,
        arrXucXac: newArrXucXac,
        tongSoLanChoi: newTongSoLanChoi,
        tongSoLanThang: newTongSoLanThang,
      };
    }
    case CHON: {
      let newLuaChon = state.luaChon;
      newLuaChon = payload;
      return { ...state, luaChon: newLuaChon };
    }

    default:
      return state;
  }
};
