import React, { Component } from "react";
import { connect } from "react-redux";
import { CHON, TAI, XIU } from "./redux/Constants/xucXacConstant";
class XucXac extends Component {
  state = {
    active: null,
  };
  handleChangeActive = (value) => {
    this.setState({ active: value });
  };
  renderListXucXac = () => {
    return this.props.arrXucXac.map((item, index) => {
      return (
        <img
          src={item.img}
          key={index}
          style={{ width: 100, margin: 10, borderRadius: 8 }}
          alt=""
        />
      );
    });
  };

  render() {
    let { active } = this.state;
    return (
      <div className="d-flex justify-content-between container pt-5">
        <button
          style={{
            width: 150,
            height: 150,
            fontSize: 40,
            transform: `scale(${active == TAI ? 1 : 0.5})`,
          }}
          onClick={() => {
            this.handleChangeActive(TAI);
            this.props.handleChoose(TAI);
          }}
          className="btn btn-danger"
        >
          Tài
        </button>
        <div>{this.renderListXucXac()}</div>
        <button
          style={{
            width: 150,
            height: 150,
            fontSize: 40,
            transform: `scale(${active == XIU ? 1 : 0.5})`,
          }}
          onClick={() => {
            this.handleChangeActive(XIU);
            this.props.handleChoose(XIU);
          }}
          className="btn btn-dark"
        >
          Xỉu
        </button>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  arrXucXac: state.xucXacReducer.arrXucXac,
});
const mapDispatchToProps = (dispatch) => {
  return {
    handleChoose: (value) => {
      dispatch({
        type: CHON,
        payload: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
